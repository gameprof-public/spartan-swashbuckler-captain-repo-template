# Spartan Swashbuckler Captain Repo Template

## Getting started

**You should create a new project from this template to incorporate your Spartan Swashbuckler captain ScriptableObject and other files into the project.**

This repo is at:

* **[https://tinyurl.com/mi431-captain-repo-template](https://tinyurl.com/mi431-captain-repo-template)** or
* **[https://gitlab.msu.edu/gameprof-public/spartan-swashbuckler-captain-repo-template](https://gitlab.msu.edu/gameprof-public/spartan-swashbuckler-captain-repo-template)**

### Steps:
1. Click the *Code* button to the right of the main page of this repo and copy the *Clone with HTTPS* link.
2. On the [Homepage of GitLab](http://gitlab.msu.edu), click *New Project*.
3. Choose *Import project* from the list of 4 options.
4. Click *Repository by URL*.
5. Paste the link to this project into the *Git repository URL* field. The correct link is: *https://gitlab.msu.edu/gameprof-public/spartan-swashbuckler-captain-repo-template.git*
6. **Correctly name your project!** Set the project name to **[YourInitials]-[TheYear]-Captain-[Your_Name]**. For example: **JGB-24-Captain-Jeremy\_Bond**
7. Choose your name under the Users section of the Project URL pop-up.
8. Make sure that the visibility is *Private*.
9. Click *Create Project*.
10. Pull down your new GitLab repo to your personal computer and place the folder for it inside of the `____Getting Started / Private Items (not pushed to Cloud)` folder.
11. Be sure to add me ( @gameprof ) and your ULA ( @hughe3911 ) as Owners on your repo so that we can pull from it.


## Things to Include in Your Folder (i.e., This Repo)

* Your **Captain AI C# file**. This should extend the `CaptainStrategy_XnAI_SO` class. Your C# file should be named something like **CS\_[YourInitials]\_[BriefDescription].cs**. For example: `CS_JGB_Pirate2_SO.cs`.
  * Note: The `.cs` file extension will not appear in the Unity Project pane
  * Note: You don't need to have `_SO` at the end. I just tend to use that to show that it's a ScriptableObject.
* Your **Captain AI ScriptableObject Instance**. This is an instance of your C# file with the behavior tree set up. It should be named using the same naming convention as your C# file. For example: `CS_JGB_Pirate2_SO`.
* Your **Flag image**:
  * Size: 512x512
  * Format: PNG
  * Naming Convention: **Flag\_[Initials]**. (e.g., `Flag_JGB`)
* Your **Behavior Tree diagram** (see below).
  * This should be a **PNG image of your diagram**.
  * If you can, please also include the actual file for the software you used to make it, but for online documents like Lucidchart, that won't exist.
* Your personal copy (or copies) of the `___Settings` ScriptableObject. (e.g., `JGB Settings`). To make your own version of the settings:
  * Duplicate the `___Settings` ScriptableObject instance in the `____Getting Started` folder.
  * Place it into your personal folder (i.e., this repo).
  * Edit it to add or remove any captains you want to see (watch my video about this from April 8, 2024).
* Your **colors description as HTML** (see below). (e.g., `Colors_JGB.html`)


## Behavior Tree Diagram
There are several online tools you can use to draw these diagrams, including:

* [**Lucidchart**](http://lucidchart.com)
* [**Miro**](http://miro.com)
* [**Draw.io**](http://draw.io)
* I built mine in [**OmniGraffle**](https://www.omnigroup.com/omnigraffle) for macOS, but any of the above software will do.

The following image shows the types of nodes you can have in your diagram and what shapes they should be. I built it in Lucidchart.

![Behavior_Tree_Nodes.png](Behavior_Tree_Nodes.png "Behavior_Tree_Nodes.png")

## Swashbuckler Colors HTML File
You should pick your colors for your swashbuckler and include them in an HTML file. Here is example text of the HTML file:

```
<style>
    body {
        font-family: sans-serif;
    }
    table, th, td {
        border: 1px solid black;
    }
    th, td {
        padding: 10px;
    }
</style>

<html><body>
<h2>Colors for JGB Jeremy Gibson Bond:</h2>
<table>
    <tr>
    <td bgcolor= 18453B><b>Primary</b></td>
    <td bgcolor=FFFFFF><b>Secondary</b></td>
    </tr>
    <tr>
    <td><b>18453B</b></td>
    <td><b>FFFFFF</b></td>
    </tr>
</table>
</body></html>
```


### To show your colors, replace the following in the html code above:
1. Replace the initials and name with your own (e.g., *not* JGB Jeremy Gibson Bond).
2. Replace *both* instances of "18453B" with your primary color.
3. Replace *both* instances of "#FFFFFF" with your secondary color.

### The HTML file you create will look something like the following:

<h3>Colors for JGB Jeremy Gibson Bond:</h3>
<table>
    <tr>
    <td bgcolor= 18453B><b>Primary</b></td>
    <td bgcolor=FFFFFF><b>Secondary</b></td>
    </tr>
    <tr>
    <td><b>18453B</b></td>
    <td><b>FFFFFF</b></td>
    </tr>
</table>

*Note that the colored backgrounds of the cells don't show up on the GitLab website.*